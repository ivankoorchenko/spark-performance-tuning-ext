## Spark Performance Foundations
Further readings and references:

https://spark.apache.org/docs/3.0.1/configuration.html
See `spark.blacklist.*` number of configurations to familiarize more with blacklisted executors.

More about "WholeStageCodegen" or what are the job execution process:
https://www.databricks.com/blog/2015/04/13/deep-dive-into-spark-sqls-catalyst-optimizer.html

Spark UI docs:
https://spark.apache.org/docs/latest/web-ui.html

More about spark properties you can find at documentations:
https://spark.apache.org/docs/latest/configuration.html#spark-properties
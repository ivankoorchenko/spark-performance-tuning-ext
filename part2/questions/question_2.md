**Given**:
<br>There are two CSV files with organization employees and department data:
- `emp.csv` with headers: `id,full_name,dep_no,salary,date_joined,date_left`;
- `deps.csv` with headers: `id,dep_name,date_found`;

**Question**:
Which piece of the code corresponds to the following Spark query execution plan?

```text
== Physical Plan ==
TakeOrderedAndProject(limit=1, orderBy=[dep_budget#81 DESC NULLS LAST], output=[id#44,dep_name#45,dep_budget#81])
+- *(3) HashAggregate(keys=[id#44, dep_name#45], functions=[sum(cast(salary#19 as double))])
   +- Exchange hashpartitioning(id#44, dep_name#45, 200), true, [id=#165]
      +- *(2) HashAggregate(keys=[id#44, dep_name#45], functions=[partial_sum(cast(salary#19 as double))])
         +- *(2) Project [salary#19, id#44, dep_name#45]
            +- *(2) BroadcastHashJoin [dep_no#18], [id#44], Inner, BuildRight
               :- *(2) Project [dep_no#18, salary#19]
               :  +- *(2) Filter isnotnull(dep_no#18)
               :     +- FileScan csv [dep_no#18,salary#19] Batched: false, DataFilters: [isnotnull(dep_no#18)], Format: CSV, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(dep_no)], ReadSchema: struct<dep_no:string,salary:string>
               +- BroadcastExchange HashedRelationBroadcastMode(List(input[0, string, true])), [id=#159]
                  +- *(1) Project [id#44, dep_name#45]
                     +- *(1) Filter isnotnull(id#44)
                        +- FileScan csv [id#44,dep_name#45] Batched: false, DataFilters: [isnotnull(id#44)], Format: CSV, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(id)], ReadSchema: struct<id:string,dep_name:string>
```

Option 1:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
val departmentsDF = spark.read.option("header", "true").csv("deps.csv")
employeesDF
  .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
  .groupBy(departmentsDF("id"), departmentsDF("dep_name"))
  .agg(sum("salary").cast(IntegerType).as("dep_budget"))
  .orderBy(desc("dep_budget"))
  .limit(1)
```

Option 2:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
val departmentsDF = spark.read.option("header", "true").csv("deps.csv")
employeesDF
  .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
  .filter(length(employeesDF("date_left")) !== 0)
  .groupBy(departmentsDF("id"), departmentsDF("dep_name"))
  .agg(count("*").as("dep_people_left"))
  .orderBy(desc("dep_people_left"))
  .limit(1)
```

Option 3:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
employeesDF
  .filter(employeesDF("date_left").isNull)
  .agg(sum("salary"))
  .limit(1)
```

Answer: Option 1

Explanation:
You can notice `functions=[partial_sum(cast(salary#19 as double))]` in `HashAggregate` of execution plan,
which means that `sum` for `salary` column was applied for aggregation.
The `BroadcastHashJoin` step in the plan tells that join occur.
This all means that first job which includes joining between two Dataframes and subsequent sum by salary is the right answer.


For `Option 2` the Execution Plan looks following:
```
== Physical Plan ==
TakeOrderedAndProject(limit=1, orderBy=[dep_people_left#82L DESC NULLS LAST], output=[id#44,dep_name#45,dep_people_left#82L])
+- *(3) HashAggregate(keys=[id#44, dep_name#45], functions=[count(1)])
   +- Exchange hashpartitioning(id#44, dep_name#45, 200), true, [id=#165]
      +- *(2) HashAggregate(keys=[id#44, dep_name#45], functions=[partial_count(1)])
         +- *(2) Project [id#44, dep_name#45]
            +- *(2) BroadcastHashJoin [dep_no#18], [id#44], Inner, BuildRight
               :- *(2) Project [dep_no#18]
               :  +- *(2) Filter (NOT (length(date_left#21) = 0) AND isnotnull(dep_no#18))
               :     +- FileScan csv [dep_no#18,date_left#21] Batched: false, DataFilters: [NOT (length(date_left#21) = 0), isnotnull(dep_no#18)], Format: CSV, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(dep_no)], ReadSchema: struct<dep_no:string,date_left:string>
               +- BroadcastExchange HashedRelationBroadcastMode(List(input[0, string, true])), [id=#159]
                  +- *(1) Project [id#44, dep_name#45]
                     +- *(1) Filter isnotnull(id#44)
                        +- FileScan csv [id#44,dep_name#45] Batched: false, DataFilters: [isnotnull(id#44)], Format: CSV, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(id)], ReadSchema: struct<id:string,dep_name:string>
```

For `Option 2` it looks simpler comparing to the other two:
```
== Physical Plan ==
CollectLimit 1
+- *(2) HashAggregate(keys=[], functions=[sum(cast(salary#19 as double))])
   +- Exchange SinglePartition, true, [id=#92]
      +- *(1) HashAggregate(keys=[], functions=[partial_sum(cast(salary#19 as double))])
         +- *(1) Project [salary#19]
            +- *(1) Filter isnull(date_left#21)
               +- FileScan csv [salary#19,date_left#21] Batched: false, DataFilters: [isnull(date_left#21)], Format: CSV, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNull(date_left)], ReadSchema: struct<salary:string,date_left:string>
```

**Question**:
Stage identifier unique at which level:

**Options**:
- Per job;
- Per run;
- Per application (correct);

**Question**:
Blacklisted executor is an executor that:

**Options**:
- Spend too much time in GC; 
- Failed a lot of tasks; (correct);
- Slower the other executors;

**Question**:
The "Tasks: Succeeded/Total" column at stage page "Completed stages" table corresponds also to:

**Options**:
- Number of partitions; (correct);
- Number of narrow transformations within a stage;
- Number of wide transformations done before the current stage;

**Question**:
"Exchange" title in blue boxes in Spark UI for DAG identifies:

**Options**:
- `reparation` method invocation;
- Any operation that requires shuffle; (correct)
- `join` of two RDD's executed;

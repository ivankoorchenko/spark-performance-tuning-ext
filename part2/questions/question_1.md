**Given**:
<br>There are two CSV files with organization employees and department data:
- `emp.csv` with headers: `id,full_name,dep_no,salary`;
- `deps.csv` with headers: `id,dep_name,date_found`;

**Question**:
Which piece of the code corresponds to the following Spark UI screenshot?

![question_1.png](images/question_1/option_2.png)

Option 1:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
val departmentsDF = spark.read.option("header", "true").csv("deps.csv")
employeesDF
  .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
  .groupBy(departmentsDF("id"))
  .agg(avg("salary").cast(IntegerType))
```

Option 2:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
val departmentsDF = spark.read.option("header", "true").csv("deps.csv")
employeesDF
  .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
  .groupBy(departmentsDF("id"), departmentsDF("dep_name"))
  .agg(count(employeesDF("id")).as("dep_size"))
  .orderBy(desc("dep_size"))
  .limit(1)
  .select("dep_name")
```


Option 3:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
val window = Window.partitionBy("dep_no").orderBy(desc("salary"))

employeesDF
  .withColumn("salary_rank", rank().over(window))
  .where(col("salary_rank") <= 2)
  .drop("salary_rank")
```

Answer: Option 2

Explanations:

There is `takeOrdered` step at second stage of a job. This step corresponds to `limit` operator, which present only 
in the second job.

For `Option 1` DAG in a Spark UI looks in the following way: ![](images/question_1/option_1.png)
and for `Option 3` representation is the following:![](images/question_1/option_3.png) 
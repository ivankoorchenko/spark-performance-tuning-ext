## Further readings and references:

### Spark configurations
More about spark properties you can find at documentations:
https://spark.apache.org/docs/3.0.1/configuration.html
See `spark.blacklist.*` number of configurations to familiarize more with blacklisted executors.

### Spark code generation
More about "WholeStageCodegen" or what are the job execution process:
https://www.databricks.com/blog/2015/04/13/deep-dive-into-spark-sqls-catalyst-optimizer.html

### Spark UI documentation
Spark UI docs contains necessary details about this visualisation tool:
https://spark.apache.org/docs/latest/web-ui.html

### Spark official recommendations about performance tuning
Performance Tuning Spark documentation:
https://spark.apache.org/docs/latest/sql-performance-tuning.html

### Databricks bucketing example
Bucketing example by Databricks:
https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/4861715144695760/2994977456373837/5701837197372837/latest.html


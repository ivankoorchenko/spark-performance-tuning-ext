# Final exam

This examination consists of 16 questions and covers topics from different parts of the course.

### Question #1

**Given**:
<br>There are two CSV files with organization employees and department data:
- `emp.csv` with headers: `id,full_name,dep_no,salary`;
- `deps.csv` with headers: `id,dep_name,date_found`;

**Question**:

Which piece of the code corresponds to the following Spark UI screenshot?

![question_1.png](images/question_1/option_2.png)

**Options**:

Option 1:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
val departmentsDF = spark.read.option("header", "true").csv("deps.csv")
employeesDF
  .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
  .groupBy(departmentsDF("id"))
  .agg(avg("salary").cast(IntegerType))
```

Option 2:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
val departmentsDF = spark.read.option("header", "true").csv("deps.csv")
employeesDF
  .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
  .groupBy(departmentsDF("id"), departmentsDF("dep_name"))
  .agg(count(employeesDF("id")).as("dep_size"))
  .orderBy(desc("dep_size"))
  .limit(1)
  .select("dep_name")
```


Option 3:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
val window = Window.partitionBy("dep_no").orderBy(desc("salary"))

employeesDF
  .withColumn("salary_rank", rank().over(window))
  .where(col("salary_rank") <= 2)
  .drop("salary_rank")
```

**Answer**:

Option 2

**Explanations**:

There is `takeOrdered` step at second stage of a job. This step corresponds to `limit` operator, which present only
in the second job.

For `Option 1` DAG in a Spark UI looks in the following way: 

![](images/question_1/option_1.png)

and for `Option 3` representation is the following:

![](images/question_1/option_3.png) 

### Question #2

**Given**:
<br>There are two CSV files with organization employees and department data:
- `emp.csv` with headers: `id,full_name,dep_no,salary,date_joined,date_left`;
- `deps.csv` with headers: `id,dep_name,date_found`;

**Question**:
Which piece of the code corresponds to the following Spark query execution plan?

```text
== Physical Plan ==
TakeOrderedAndProject(limit=1, orderBy=[dep_budget#81 DESC NULLS LAST], output=[id#44,dep_name#45,dep_budget#81])
+- *(3) HashAggregate(keys=[id#44, dep_name#45], functions=[sum(cast(salary#19 as double))])
   +- Exchange hashpartitioning(id#44, dep_name#45, 200), true, [id=#165]
      +- *(2) HashAggregate(keys=[id#44, dep_name#45], functions=[partial_sum(cast(salary#19 as double))])
         +- *(2) Project [salary#19, id#44, dep_name#45]
            +- *(2) BroadcastHashJoin [dep_no#18], [id#44], Inner, BuildRight
               :- *(2) Project [dep_no#18, salary#19]
               :  +- *(2) Filter isnotnull(dep_no#18)
               :     +- FileScan csv [dep_no#18,salary#19] Batched: false, DataFilters: [isnotnull(dep_no#18)], Format: CSV, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(dep_no)], ReadSchema: struct<dep_no:string,salary:string>
               +- BroadcastExchange HashedRelationBroadcastMode(List(input[0, string, true])), [id=#159]
                  +- *(1) Project [id#44, dep_name#45]
                     +- *(1) Filter isnotnull(id#44)
                        +- FileScan csv [id#44,dep_name#45] Batched: false, DataFilters: [isnotnull(id#44)], Format: CSV, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(id)], ReadSchema: struct<id:string,dep_name:string>
```

Option 1:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
val departmentsDF = spark.read.option("header", "true").csv("deps.csv")
employeesDF
  .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
  .groupBy(departmentsDF("id"), departmentsDF("dep_name"))
  .agg(sum("salary").cast(IntegerType).as("dep_budget"))
  .orderBy(desc("dep_budget"))
  .limit(1)
```

Option 2:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
val departmentsDF = spark.read.option("header", "true").csv("deps.csv")
employeesDF
  .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
  .filter(length(employeesDF("date_left")) !== 0)
  .groupBy(departmentsDF("id"), departmentsDF("dep_name"))
  .agg(count("*").as("dep_people_left"))
  .orderBy(desc("dep_people_left"))
  .limit(1)
```

Option 3:
```scala
val employeesDF = spark.read.option("header", "true").csv("emp.csv")
employeesDF
  .filter(employeesDF("date_left").isNull)
  .agg(sum("salary"))
  .limit(1)
```

**Answer**:

Option 1

**Explanations**:

You can notice `functions=[partial_sum(cast(salary#19 as double))]` in `HashAggregate` of execution plan,
which means that `sum` for `salary` column was applied for aggregation.
The `BroadcastHashJoin` step in the plan tells that join occur.
This all means that first job which includes joining between two Dataframes and subsequent sum by salary is the right answer.


For `Option 2` the Execution Plan looks following:
```
== Physical Plan ==
TakeOrderedAndProject(limit=1, orderBy=[dep_people_left#82L DESC NULLS LAST], output=[id#44,dep_name#45,dep_people_left#82L])
+- *(3) HashAggregate(keys=[id#44, dep_name#45], functions=[count(1)])
   +- Exchange hashpartitioning(id#44, dep_name#45, 200), true, [id=#165]
      +- *(2) HashAggregate(keys=[id#44, dep_name#45], functions=[partial_count(1)])
         +- *(2) Project [id#44, dep_name#45]
            +- *(2) BroadcastHashJoin [dep_no#18], [id#44], Inner, BuildRight
               :- *(2) Project [dep_no#18]
               :  +- *(2) Filter (NOT (length(date_left#21) = 0) AND isnotnull(dep_no#18))
               :     +- FileScan csv [dep_no#18,date_left#21] Batched: false, DataFilters: [NOT (length(date_left#21) = 0), isnotnull(dep_no#18)], Format: CSV, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(dep_no)], ReadSchema: struct<dep_no:string,date_left:string>
               +- BroadcastExchange HashedRelationBroadcastMode(List(input[0, string, true])), [id=#159]
                  +- *(1) Project [id#44, dep_name#45]
                     +- *(1) Filter isnotnull(id#44)
                        +- FileScan csv [id#44,dep_name#45] Batched: false, DataFilters: [isnotnull(id#44)], Format: CSV, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(id)], ReadSchema: struct<id:string,dep_name:string>
```

For `Option 2` it looks simpler comparing to the other two:
```
== Physical Plan ==
CollectLimit 1
+- *(2) HashAggregate(keys=[], functions=[sum(cast(salary#19 as double))])
   +- Exchange SinglePartition, true, [id=#92]
      +- *(1) HashAggregate(keys=[], functions=[partial_sum(cast(salary#19 as double))])
         +- *(1) Project [salary#19]
            +- *(1) Filter isnull(date_left#21)
               +- FileScan csv [salary#19,date_left#21] Batched: false, DataFilters: [isnull(date_left#21)], Format: CSV, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNull(date_left)], ReadSchema: struct<salary:string,date_left:string>
```

### Question #4
**Question**:
Stage identifier unique at which level:

**Options**:
- Per job;
- Per run;
- Per application (correct);

### Question #5
**Question**:
Blacklisted executor is an executor that:

**Options**:
- Spend too much time in GC;
- Failed a lot of tasks; (correct);
- Slower than other executors;

### Question #6
**Question**:
The "Tasks: Succeeded/Total" column at stage page "Completed stages" table corresponds also to:

**Options**:
- Number of partitions; (correct);
- Number of narrow transformations within a stage;
- Number of wide transformations done before the current stage;

### Question #7
**Question**:
"Exchange" title in blue boxes on the Spark UI for DAG identifies:

**Options**:
- `reparation` method invocation;
- Any operation that requires shuffle; (correct)
- `join` of two RDD's execution;

**Explanation**:
"Exchange" on the spark UI means a shuffle, regardless of what caused it: join, repartition, etc.

### Question #8
What Datasets or RDD's need to have in common to avoid shuffle for join?
- Partitioner; (correct)
- Number of partitions;
- Columns;

**Explanation**:
`org.apache.spark.Partitioner` interface has method `numPartitions` to identify number of partitions and
`getPartition` to identify which partition a value belongs to. Hence, Datasets or RDD's need to have a common partitioner
so Spark knows that the same data already belongs to the same partition and shuffle can be skipped.


### Question #9
**Question**:
In order to join two Datasets or RDD's with the same key, should be:

**Options**:
To join two Datasets or RDD's by the same column or key without a shuffle,
Spark needs to make sure that rows with the same value are:
- On the same executor;
- On the same partition; (correct)
- On the same physical disk;

**Explanation**:
Spark operates in terms of partitions in the context of joins. A physical location of partitions is not necessary.

### Question #10
**Question**:
Co-partitioned RDDs are the RDDs that?:

**Options**:
- Have the same number of partitions;
- Have the same partitioner; (correct)
- Have at least one common column;

**Explanation**:
`org.apache.spark.Partitioner` interface has method `numPartitions` that returns number of partitions. 
This implies that co-partitioned also have same number of partitions. 


### Question #11
**Question**:
Which units of measurement are used in `spark.sql.autoBroadcastJoinThreshold` configuration:

**Options**:
- Size of a RDD in bytes; (correct)
- Number of rows in a RDD;
- Percentage difference between two RDDs to be joined;

**Explanation**:
Visit documentation for more details: https://spark.apache.org/docs/latest/sql-performance-tuning.html#other-configuration-options

### Question #12
**Question**:
Bucketing can be applied for:

**Options**:
- Any Dataframes or RDDs;
- Tables only; (correct)
- Files only;

**Explanation**:
As of the Spark version `3.0.2`, bucketing can be applied for Hive tables only.
For example, there is a job that writes Dataframe `df` to parquet field:
```scala
df.write.bucketBy(10, "column").parquet("data")
```

During execution, the following exception will be thrown:
```
org.apache.spark.sql.AnalysisException: 'save' does not support bucketBy right now;
```

### Question #13
**Question**:
Bucketing technique can be used to optimize which aspects of a Spark job? Check 3 options:

**Options**:
- Sorting Dataframes and RDDs;
- Filtering Dataframes and RDDs; (correct)
- Grouping and aggregation Dataframes and RDDs; (correct)
- Saving Dataframes and RDDs to files or tables;
- Joining Dataframes and RDDs; (correct)

**Explanation**:
Similarly to `repartition`, `bucketBy` will put rows with same column values it is bucketed by under same location
path. As a result, Spark can skip files to read for filtering, aggregations, and joining.  


### Question #14
**Question**:
There are two Dataframes:
- `employees` Dataframe with columns: `id, firstName, secondName, depNo`
- `deps` Dataframe with columns: `depNo, name`

Additionally, `employees` has 1M rows, `deps` 100 rows and not all `depNo` column values present in both Dataframes.
There is the following Spark job:
```scala
employees
  .withColumn("fullName", concat(employees("firstName"), employees("secondName")) )
  .join(deps, "depNo")
```

How it is possible to optimize this job using column pruning technique?

**Options**:

Option 1: Job is already optimal;

Option 2:
Select `id` column after join, which is not used:
```scala
employees
  .join(deps, "depNo")
  .drop(employees("id"))
  .withColumn("fullName", concat(employees("firstName"), employees("secondName")))
```

Option 3:
Create new column before join:
```scala
employees
  .withColumn("fullName", concat(employees("firstName"), employees("secondName")) )
  .join(deps, "depNo")
```

**Answer**:
Option 2

**Explanation**:

Optimized job:
```scala
employees
  .join(deps, "depNo")
  .withColumn("fullName", concat(employees("firstName"), employees("secondName")) )
```

Produces the following execution plan:
```
== Physical Plan ==
*(1) Project [depNo#7, id#4, firstName#5, secondName#6, name#20, concat(firstName#5, secondName#6) AS fullName#31]
+- *(1) BroadcastHashJoin [depNo#7], [depNo#19], Inner, BuildRight
   :- *(1) LocalTableScan [id#4, firstName#5, secondName#6, depNo#7]
   +- BroadcastExchange HashedRelationBroadcastMode(List(cast(input[0, int, false] as bigint))), [id=#42]
      +- LocalTableScan [depNo#19, name#20]
```
You can notice that column has been created after join. Hence `concact` will be applied to smaller resulting dataframe.


Initial job:
```scala
employees
  .join(deps, "depNo")
  .withColumn("fullName", concat(employees("firstName"), employees("secondName")) )
```

Produces the following execution plan:
```
== Physical Plan ==
*(1) Project [depNo#7, id#4, firstName#5, secondName#6, fullName#26, name#20]
+- *(1) BroadcastHashJoin [depNo#7], [depNo#19], Inner, BuildRight
   :- *(1) LocalTableScan [id#4, firstName#5, secondName#6, depNo#7, fullName#26] <- Additional `fullName` column 
   +- BroadcastExchange HashedRelationBroadcastMode(List(cast(input[0, int, false] as bigint))), [id=#42]
      +- LocalTableScan [depNo#19, name#20]
```
Where `concact` will be applied to larger dataframe, before the join.


### Question #15
**Question**:
There are two Dataframes:
- `employees` Dataframe with columns: `id, firstName, secondName, salary, depNo`
- `deps` Dataframe with columns: `depNo, name, depName`

Both Dataframes are written to `employees` and `deps` Hive tables respectively.
Later those tables are used for the following Spark job:
```scala
val employeesDF = spark.read.table("employees")
val depsDF = spark.read.table("deps")

employeesDF
  .filter(col("salary") >= 100000)
  .join(depsDF, "depNo")
  .groupBy("depName")
  .agg(count("*"))
```

What is the preferable way to store `employees` and `deps` Dataframes into tables so the job would be optimal?

**Options**:

Option 1:
```scala
employees
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "salary")
  .saveAsTable("employees")

deps
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "depNo")
  .saveAsTable("deps")
```

Option 2:
```scala
employees
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "salary", "depNo")
  .saveAsTable("employees")

deps
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "depNo")
  .saveAsTable("deps")
```

Option 3:
```scala
employees
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "depNo")
  .saveAsTable("employees")

deps
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "depNo")
  .saveAsTable("deps")
```

**Answer**:
Option 3

**Explanations**:

Execution plan for Option 1:
```
== Physical Plan ==
*(6) HashAggregate(keys=[depName#48], functions=[count(1)])
+- Exchange hashpartitioning(depName#48, 200), true, [id=#186]
   +- *(5) HashAggregate(keys=[depName#48], functions=[partial_count(1)])
      +- *(5) Project [depName#48]
         +- *(5) SortMergeJoin [depNo#41], [depNo#47], Inner
            :- *(2) Sort [depNo#41 ASC NULLS FIRST], false, 0
            :  +- Exchange hashpartitioning(depNo#41, 200), true, [id=#167]
            :     +- *(1) Project [depNo#41]
            :        +- *(1) Filter ((isnotnull(salary#40) AND (salary#40 >= 100000)) AND isnotnull(depNo#41))
            :           +- *(1) ColumnarToRow
            :              +- FileScan parquet default.employees[salary#40,depNo#41] Batched: true, DataFilters: [isnotnull(salary#40), (salary#40 >= 100000), isnotnull(depNo#41)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(salary), GreaterThanOrEqual(salary,100000), IsNotNull(depNo)], ReadSchema: struct<salary:int,depNo:int>, SelectedBucketsCount: 5 out of 5
            +- *(4) Sort [depNo#47 ASC NULLS FIRST], false, 0
               +- Exchange hashpartitioning(depNo#47, 200), true, [id=#177]
                  +- *(3) Project [depNo#47, depName#48]
                     +- *(3) Filter isnotnull(depNo#47)
                        +- *(3) ColumnarToRow
                           +- FileScan parquet default.deps[depNo#47,depName#48] Batched: true, DataFilters: [isnotnull(depNo#47)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(depNo)], ReadSchema: struct<depNo:int,depName:string>, SelectedBucketsCount: 5 out of 5
```

Execution plan for Option 2:
```
== Physical Plan ==
*(6) HashAggregate(keys=[depName#48], functions=[count(1)])
+- Exchange hashpartitioning(depName#48, 200), true, [id=#186]
   +- *(5) HashAggregate(keys=[depName#48], functions=[partial_count(1)])
      +- *(5) Project [depName#48]
         +- *(5) SortMergeJoin [depNo#41], [depNo#47], Inner
            :- *(2) Sort [depNo#41 ASC NULLS FIRST], false, 0
            :  +- Exchange hashpartitioning(depNo#41, 200), true, [id=#167]
            :     +- *(1) Project [depNo#41]
            :        +- *(1) Filter ((isnotnull(salary#40) AND (salary#40 >= 100000)) AND isnotnull(depNo#41))
            :           +- *(1) ColumnarToRow
            :              +- FileScan parquet default.employees[salary#40,depNo#41] Batched: true, DataFilters: [isnotnull(salary#40), (salary#40 >= 100000), isnotnull(depNo#41)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(salary), GreaterThanOrEqual(salary,100000), IsNotNull(depNo)], ReadSchema: struct<salary:int,depNo:int>, SelectedBucketsCount: 5 out of 5
            +- *(4) Sort [depNo#47 ASC NULLS FIRST], false, 0
               +- Exchange hashpartitioning(depNo#47, 200), true, [id=#177]
                  +- *(3) Project [depNo#47, depName#48]
                     +- *(3) Filter isnotnull(depNo#47)
                        +- *(3) ColumnarToRow
                           +- FileScan parquet default.deps[depNo#47,depName#48] Batched: true, DataFilters: [isnotnull(depNo#47)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(depNo)], ReadSchema: struct<depNo:int,depName:string>, SelectedBucketsCount: 5 out of 5

```

Execution plan for Option 3:
```
== Physical Plan ==
 *(4) HashAggregate(keys=[depName#48], functions=[count(1)])
+- Exchange hashpartitioning(depName#48, 200), true, [id=#166]
   +- *(3) HashAggregate(keys=[depName#48], functions=[partial_count(1)])
      +- *(3) Project [depName#48]
         +- *(3) SortMergeJoin [depNo#41], [depNo#47], Inner
            :- *(1) Sort [depNo#41 ASC NULLS FIRST], false, 0
            :  +- *(1) Project [depNo#41]
            :     +- *(1) Filter ((isnotnull(salary#40) AND (salary#40 >= 100000)) AND isnotnull(depNo#41))
            :        +- *(1) ColumnarToRow
            :           +- FileScan parquet default.employees[salary#40,depNo#41] Batched: true, DataFilters: [isnotnull(salary#40), (salary#40 >= 100000), isnotnull(depNo#41)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(salary), GreaterThanOrEqual(salary,100000), IsNotNull(depNo)], ReadSchema: struct<salary:int,depNo:int>, SelectedBucketsCount: 5 out of 5
            +- *(2) Sort [depNo#47 ASC NULLS FIRST], false, 0
               +- *(2) Project [depNo#47, depName#48]
                  +- *(2) Filter isnotnull(depNo#47)
                     +- *(2) ColumnarToRow
                        +- FileScan parquet default.deps[depNo#47,depName#48] Batched: true, DataFilters: [isnotnull(depNo#47)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(depNo)], ReadSchema: struct<depNo:int,depName:string>, SelectedBucketsCount: 5 out of 5
```

Pay attention to `Exchange hashpartitioning` task - only for a third option it occurs once, while for another two it
occurs twice because dataframes are not co-partitioned.



### Question #16
**Question**:
There are two Dataframes:
- `employees` Dataframe with columns: `id, firstName, secondName, salary, depNo`
- `deps` Dataframe with columns: `depNo, depName`

About 50% of `employees` rows contains `depNo` 1.
It is needed to find number of employees that belongs to all departments with name `"Innovation Lab"`
Choose the most optimal way.

**Options**:

Options 1:
```scala
employees
  .join(deps, "depNo")
  .where(col("depName") === "Innovation Lab")
  .agg(count("*"))
```

Option 2:
```scala
employees
  .repartition(col("depNo"))
  .join(deps.repartition(col("depNo")), "depNo")
  .where(col("depName") === "Innovation Lab")
  .agg(count("*"))
```

Option 2:
```scala
employees
  .repartition(col("depNo"))
  .join(deps.repartition(col("depNo")), "depNo")
  .where(col("depName") === "Innovation Lab")
  .agg(count("*"))
```

Option 3:
```scala
def partitionColumn(df: DataFrame) = {
  df.withColumn("depPartition", col("depNo") % 24)
    .repartition(col("depPartition"))
}

partitionColumn(employees)
  .join(partitionColumn(deps), "depPartition")
  .where(col("employees.depNo") === col("deps.depNo"))
  .where(col("depName") === "Innovation Lab")
  .agg(count("*"))
```

**Answer**:
Option 2

**Explanation**:
Option 1 joins two Dataframes which are NOT co-partitioned.
Option 2 joins two Dataframes which are co-partitioned. Despite the fact that, it causes additional shuffles final
join because this is narrow and much faster.
Option 3 is an attempt to tackle data skew issue, but it does not work because `depNo=1` `employee` rows still appears
in the same partition, hence it causes shuffle without further benefits and slower than Option 2.

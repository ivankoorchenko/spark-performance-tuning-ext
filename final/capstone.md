### Capstone project

In this project, you will be able to apply most of the knowledge you have gained during the course.

Given the following project: there is some organization with departments and employees.
The organization has a remote API that allows to get data about those.
The goal of one of the Spark jobs is to keep data in sync regularly, for instance, once a day. 
Later, we need to analyze this data to get some insights, for instance, once a week.
As you may notice, sync job is more frequent than the analysis job.
Intermediate storage is used to avoid hitting the remote API too often and can be considered as a small Data Warehouse.

It consists of two Spark jobs:
- `OrganizationDataSyncApplication` - its purpose to sync data from the remote API to the local storage.
- `OrganizationDataAnalysisApplication` - its purpose to analyze the data from the local storage.

`OrganizationDataSyncApplication` should be executed first and then `OrganizationDataAnalysisApplication`.

The goal is to apply various techniques which you have learned during the course and 
to find out which of them works better than others for this particular case.
This implies changes for both `OrganizationDataAnalysisApplication` and `OrganizationDataSyncApplication`.

Hints for optimizations:
* Try to use different file formats for data storage, like `parquet` instead of `json`. 
* Try to use bucketing and different number of buckets if this is applicable.
* Try to use partitioning or broadcasts joins if this is applicable.
* Try to discover whether there are data skews and find a way to handle them.
* Try to cache or persist intermediate computations.

NOTE: Please, don't change an implementation of `OrganizationDataSource` as it is a stub for the remote API.

Code is located at: `src/main/scala/capstone`
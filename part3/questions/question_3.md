There are two Dataframes:
- `employees` Dataframe with columns: `id, firstName, secondName, salary, depNo`
- `deps` Dataframe with columns: `depNo, name, depName`

Both Dataframes are written to `employees` and `deps` Hive tables respectively. 
Later those tables are used for the following Spark job:
```scala
val employeesDF = spark.read.table("employees")
val depsDF = spark.read.table("deps")

employeesDF
  .filter(col("salary") >= 100000)
  .join(depsDF, "depNo")
  .groupBy("depName")
  .agg(count("*"))
```

What is the preferable way to store `employees` and `deps` Dataframes into tables so the job would be optimal?

Option 1:
```scala
employees
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "salary")
  .saveAsTable("employees")

deps
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "depNo")
  .saveAsTable("deps")
```

Option 2:
```scala
employees
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "salary", "depNo")
  .saveAsTable("employees")

deps
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "depNo")
  .saveAsTable("deps")
```

Option 3:
```scala
employees
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "depNo")
  .saveAsTable("employees")

deps
  .write
  .mode(SaveMode.Overwrite)
  .bucketBy(5, "depNo")
  .saveAsTable("deps")
```

Answer: Option 3

Explanations:

Execution plan for Option 1:
```
== Physical Plan ==
*(6) HashAggregate(keys=[depName#48], functions=[count(1)])
+- Exchange hashpartitioning(depName#48, 200), true, [id=#186]
   +- *(5) HashAggregate(keys=[depName#48], functions=[partial_count(1)])
      +- *(5) Project [depName#48]
         +- *(5) SortMergeJoin [depNo#41], [depNo#47], Inner
            :- *(2) Sort [depNo#41 ASC NULLS FIRST], false, 0
            :  +- Exchange hashpartitioning(depNo#41, 200), true, [id=#167]
            :     +- *(1) Project [depNo#41]
            :        +- *(1) Filter ((isnotnull(salary#40) AND (salary#40 >= 100000)) AND isnotnull(depNo#41))
            :           +- *(1) ColumnarToRow
            :              +- FileScan parquet default.employees[salary#40,depNo#41] Batched: true, DataFilters: [isnotnull(salary#40), (salary#40 >= 100000), isnotnull(depNo#41)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(salary), GreaterThanOrEqual(salary,100000), IsNotNull(depNo)], ReadSchema: struct<salary:int,depNo:int>, SelectedBucketsCount: 5 out of 5
            +- *(4) Sort [depNo#47 ASC NULLS FIRST], false, 0
               +- Exchange hashpartitioning(depNo#47, 200), true, [id=#177]
                  +- *(3) Project [depNo#47, depName#48]
                     +- *(3) Filter isnotnull(depNo#47)
                        +- *(3) ColumnarToRow
                           +- FileScan parquet default.deps[depNo#47,depName#48] Batched: true, DataFilters: [isnotnull(depNo#47)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(depNo)], ReadSchema: struct<depNo:int,depName:string>, SelectedBucketsCount: 5 out of 5
```

Execution plan for Option 2:
```
== Physical Plan ==
*(6) HashAggregate(keys=[depName#48], functions=[count(1)])
+- Exchange hashpartitioning(depName#48, 200), true, [id=#186]
   +- *(5) HashAggregate(keys=[depName#48], functions=[partial_count(1)])
      +- *(5) Project [depName#48]
         +- *(5) SortMergeJoin [depNo#41], [depNo#47], Inner
            :- *(2) Sort [depNo#41 ASC NULLS FIRST], false, 0
            :  +- Exchange hashpartitioning(depNo#41, 200), true, [id=#167]
            :     +- *(1) Project [depNo#41]
            :        +- *(1) Filter ((isnotnull(salary#40) AND (salary#40 >= 100000)) AND isnotnull(depNo#41))
            :           +- *(1) ColumnarToRow
            :              +- FileScan parquet default.employees[salary#40,depNo#41] Batched: true, DataFilters: [isnotnull(salary#40), (salary#40 >= 100000), isnotnull(depNo#41)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(salary), GreaterThanOrEqual(salary,100000), IsNotNull(depNo)], ReadSchema: struct<salary:int,depNo:int>, SelectedBucketsCount: 5 out of 5
            +- *(4) Sort [depNo#47 ASC NULLS FIRST], false, 0
               +- Exchange hashpartitioning(depNo#47, 200), true, [id=#177]
                  +- *(3) Project [depNo#47, depName#48]
                     +- *(3) Filter isnotnull(depNo#47)
                        +- *(3) ColumnarToRow
                           +- FileScan parquet default.deps[depNo#47,depName#48] Batched: true, DataFilters: [isnotnull(depNo#47)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(depNo)], ReadSchema: struct<depNo:int,depName:string>, SelectedBucketsCount: 5 out of 5

```

Execution plan for Option 3:
```
== Physical Plan ==
 *(4) HashAggregate(keys=[depName#48], functions=[count(1)])
+- Exchange hashpartitioning(depName#48, 200), true, [id=#166]
   +- *(3) HashAggregate(keys=[depName#48], functions=[partial_count(1)])
      +- *(3) Project [depName#48]
         +- *(3) SortMergeJoin [depNo#41], [depNo#47], Inner
            :- *(1) Sort [depNo#41 ASC NULLS FIRST], false, 0
            :  +- *(1) Project [depNo#41]
            :     +- *(1) Filter ((isnotnull(salary#40) AND (salary#40 >= 100000)) AND isnotnull(depNo#41))
            :        +- *(1) ColumnarToRow
            :           +- FileScan parquet default.employees[salary#40,depNo#41] Batched: true, DataFilters: [isnotnull(salary#40), (salary#40 >= 100000), isnotnull(depNo#41)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(salary), GreaterThanOrEqual(salary,100000), IsNotNull(depNo)], ReadSchema: struct<salary:int,depNo:int>, SelectedBucketsCount: 5 out of 5
            +- *(2) Sort [depNo#47 ASC NULLS FIRST], false, 0
               +- *(2) Project [depNo#47, depName#48]
                  +- *(2) Filter isnotnull(depNo#47)
                     +- *(2) ColumnarToRow
                        +- FileScan parquet default.deps[depNo#47,depName#48] Batched: true, DataFilters: [isnotnull(depNo#47)], Format: Parquet, Location: InMemoryFileIndex[file:..., PartitionFilters: [], PushedFilters: [IsNotNull(depNo)], ReadSchema: struct<depNo:int,depName:string>, SelectedBucketsCount: 5 out of 5
```

Pay attention to `Exchange hashpartitioning` task - only for a third option it occurs one, while for another two it
occurs twice because dataframes are not co-partitioned.
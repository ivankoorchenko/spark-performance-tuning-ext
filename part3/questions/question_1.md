What Datasets or RDD's need to have in common to avoid shuffle for join?
- Partitioner; (correct)
- Number of partitions;
- Columns;

Explanation:
`org.apache.spark.Partitioner` interface has method `numPartitions` to identify number of partitions and 
`getPartition` to identify which partition a value belongs to. Hence, Datasets or RDD's need to have a common partitioner
so Spark knows that the same data already belongs to the same partition and shuffle can be skipped. 


In order to join two Datasets or RDD's with the same key, should be:

To join two Datasets or RDD's by the same column or key without a shuffle,
Spark needs to make sure that rows with the same value are: 
- On the same executor;
- On the same partition; (correct)
- On the same physical disk;

Explanation:
Spark operates in terms of partitions in the context of joins. A physical location of partitions is not necessary.   

Co-partitioned RDDs are the RDDs that?:
- Have the same number of partitions;
- Have the same partitioner; (correct)
- Have at least one common column;


Which units of measurement are used in `spark.sql.autoBroadcastJoinThreshold` configuration:
- Size of a RDD in bytes; (correct)
- Number of rows in a RDD;
- Percentage difference between two RDDs to be joined;

Explanation:
Visit documentation for more details: https://spark.apache.org/docs/latest/sql-performance-tuning.html#other-configuration-options

Bucketing can be applied for:
- Any Dataframes or RDDs;
- Tables only; (correct)
- Files only;

Explanation:
As of the Spark version `3.0.2`, bucketing can be applied for Hive tables only.
For example, there is a job that writes Dataframe `df` to parquet field:  
```scala
df.write.bucketBy(10, "column").parquet("data")
```

During execution, the following exception will be thrown:
```
org.apache.spark.sql.AnalysisException: 'save' does not support bucketBy right now;
```

Bucketing technique can be used to optimize which aspects of a Spark job? Check 3 options:
- Sorting Dataframes and RDDs;
- Filtering Dataframes and RDDs; (correct)
- Grouping and aggregation Dataframes and RDDs; (correct) 
- Saving Dataframes and RDDs to files or tables;
- Joining Dataframes and RDDs; (correct)

Explanations:
Similarly to `repartition`, `bucketBy` will put rows with same column values it is bucketed by under same location
path. As a result, Spark can skip files to read for filtering, aggregations, and joining.  

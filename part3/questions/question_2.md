There are two Dataframes:
- `employees` Dataframe with columns: `id, firstName, secondName, depNo`
- `deps` Dataframe with columns: `depNo, name`

Additionally, `employees` has 1M rows, `deps` 100 rows and not all `depNo` column values present in both Dataframes.
There is the following Spark job:
```scala
employees
  .withColumn("fullName", concat(employees("firstName"), employees("secondName")) )
  .join(deps, "depNo")
```

How it is possible to optimize this job using column pruning technique?

Option 1: Job is already optimal;

Option 2:
Select `id` column after join, which is not used:
```scala
employees
  .join(deps, "depNo")
  .drop(employees("id"))
  .withColumn("fullName", concat(employees("firstName"), employees("secondName")))
```

Option 3:
Create new column before join:
```scala
employees
  .withColumn("fullName", concat(employees("firstName"), employees("secondName")) )
  .join(deps, "depNo")
```

Answer: Option 2

Explanations:

Optimized job:
```scala
employees
  .join(deps, "depNo")
  .withColumn("fullName", concat(employees("firstName"), employees("secondName")) )
```

Produces the following execution plan:
```
== Physical Plan ==
*(1) Project [depNo#7, id#4, firstName#5, secondName#6, name#20, concat(firstName#5, secondName#6) AS fullName#31]
+- *(1) BroadcastHashJoin [depNo#7], [depNo#19], Inner, BuildRight
   :- *(1) LocalTableScan [id#4, firstName#5, secondName#6, depNo#7]
   +- BroadcastExchange HashedRelationBroadcastMode(List(cast(input[0, int, false] as bigint))), [id=#42]
      +- LocalTableScan [depNo#19, name#20]
```
You can notice that column has been created after join. Hence `concact` will be applied to smaller resulting dataframe.


Initial job:
```scala
employees
  .join(deps, "depNo")
  .withColumn("fullName", concat(employees("firstName"), employees("secondName")) )
```

Produces the following execution plan:
```
== Physical Plan ==
*(1) Project [depNo#7, id#4, firstName#5, secondName#6, fullName#26, name#20]
+- *(1) BroadcastHashJoin [depNo#7], [depNo#19], Inner, BuildRight
   :- *(1) LocalTableScan [id#4, firstName#5, secondName#6, depNo#7, fullName#26] <- Additional `fullName` column 
   +- BroadcastExchange HashedRelationBroadcastMode(List(cast(input[0, int, false] as bigint))), [id=#42]
      +- LocalTableScan [depNo#19, name#20]
```
Where `concact` will be applied to larger dataframe, before the join.
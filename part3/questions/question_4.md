There are two Dataframes:
- `employees` Dataframe with columns: `id, firstName, secondName, salary, depNo`
- `deps` Dataframe with columns: `depNo, depName`

About 50% of `employees` rows contains `depNo` 1. 
It is needed to find number of employees that belongs to all departments with name `"Innovation Lab"`
Choose the most optimal way.

Options 1:
```scala
employees
  .join(deps, "depNo")
  .where(col("depName") === "Innovation Lab")
  .agg(count("*"))
```

Option 2:
```scala
employees
  .repartition(col("depNo"))
  .join(deps.repartition(col("depNo")), "depNo")
  .where(col("depName") === "Innovation Lab")
  .agg(count("*"))
```

Option 2:
```scala
employees
  .repartition(col("depNo"))
  .join(deps.repartition(col("depNo")), "depNo")
  .where(col("depName") === "Innovation Lab")
  .agg(count("*"))
```

Option 3:
```scala
def partitionColumn(df: DataFrame) = {
  df.withColumn("depPartition", col("depNo") % 24)
    .repartition(col("depPartition"))
}

partitionColumn(employees)
  .join(partitionColumn(deps), "depPartition")
  .where(col("employees.depNo") === col("deps.depNo"))
  .where(col("depName") === "Innovation Lab")
  .agg(count("*"))
```

Answer: Option 2

Explanation:
Option 1 joins two Dataframes which are NOT co-partitioned.
Option 2 joins two Dataframes which are co-partitioned. Despite the fact that, it causes additional shuffles final 
join because this is narrow and much faster. 
Option 3 is an attempt to tackle data skew issue, but it does not work because `depNo=1` `employee` rows still appears 
in the same partition, hence it causes shuffle without further benefits and slower than Option 2.
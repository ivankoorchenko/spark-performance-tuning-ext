package capstone

import capstone.OrganizationDataSource.{departmentNames, jobPositions}

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import scala.util.Random

class OrganizationDataSource() {
  private val random = new Random()
  private val employeesCount = 1 * 1000000
  private val departmentsCount = 100

  /**
   * Generates random employees data.
   * @return Stream of employees.
   */
  def employees: Stream[Employee] = {
    (0 to employeesCount).toStream.map { id =>
      val firstName = s"first_name_$id"
      val secondName = s"second_name_$id"
      val salary = (id % 10 + 5) * 10000
      val depNo = pickRandomDepartment(id)

      val dateHired = LocalDate.now().minusDays(random.nextInt(365 * 5)).format(DateTimeFormatter.ISO_DATE)
      val position = pickRandomJobPosition()

      Employee(id, firstName, secondName, salary, dateHired, position, depNo)
    }
  }

  private def pickRandomDepartment(id: Int): Int = {
    if (random.nextBoolean()) {
      id % (departmentsCount - 1)
    } else {
      1
    }
  }

  private def pickRandomJobPosition(): String = {
    val randomIndex = random.nextInt(jobPositions.length)
    jobPositions(randomIndex)
  }


  /**
   * Generates random departments data.
   * @return Stream of departments.
   */
  def deps: Stream[Dep] = (1 to departmentsCount).toStream.map { id =>
    val name = departmentNames(id % departmentNames.size)
    val dateCreated = LocalDate.now().minusDays(random.nextInt(365 * 5)).format(DateTimeFormatter.ISO_DATE)
    Dep(id, name, dateCreated)
  }
}

object OrganizationDataSource {
  private val jobPositions = List(
    "Software Engineer",
    "Data Scientist",
    "Product Manager",
    "UX Designer",
    "DevOps Engineer",
    "QA Engineer",
    "Business Analyst",
    "Project Manager",
    "Sales Manager",
    "Marketing Specialist",
    "HR Manager",
    "Accountant",
    "Financial Analyst",
    "Operations Manager",
    "Customer Support Representative",
    "Graphic Designer",
    "Content Writer",
    "Network Administrator",
    "Database Administrator",
    "Systems Analyst",
    "IT Manager",
    "Security Analyst",
    "Technical Writer",
    "UI Designer",
    "Frontend Developer",
    "Backend Developer",
    "Full Stack Developer",
    "Mobile App Developer",
    "Game Developer",
    "Cloud Architect",
    "Data Engineer",
    "Machine Learning Engineer",
    "AI Researcher",
    "Blockchain Developer",
    "Cybersecurity Specialist",
    "IT Consultant",
    "Product Owner",
    "Scrum Master",
    "Data Analyst",
    "UI/UX Developer",
    "System Administrator",
    "Sales Representative",
    "Marketing Manager",
    "Content Strategist",
    "Business Development Manager",
    "Financial Controller",
    "Supply Chain Manager",
    "Operations Analyst",
    "Customer Success Manager",
    "Technical Support Engineer",
    "QA Analyst"
  )

  val departmentNames = List(
    "Innovation Lab",
    "Customer Experience Division",
    "Operations Optimization Team",
    "Sustainability Initiatives Department",
    "Data Analytics and Insights Unit"
  )
}

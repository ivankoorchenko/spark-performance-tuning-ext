package capstone

/**
 * @param id employee unique identifier
 * @param firstName employee first name
 * @param secondName employee second name
 * @param salary employee salary
 * @param dateHired date when employee was hired. Format is ISO-8601.
 * @param position employee position
 * @param depNo department number. Corresponds to Dep.depNo
 */
case class Employee(id: Long,
                    firstName: String,
                    secondName: String,
                    salary: Long,
                    dateHired: String,
                    position: String,
                    depNo: Long)

/**
 * @param depNo department number. Corresponds to Employee.depNo
 * @param depName department name
 * @param dateCreated date when department was created. Format is ISO-8601.
 */
case class Dep(depNo: Long,
               depName: String,
               dateCreated: String)


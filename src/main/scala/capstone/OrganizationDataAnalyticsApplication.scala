package capstone

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import java.time.{Duration, LocalDate, LocalDateTime}
import java.time.format.DateTimeFormatter
import scala.io.StdIn

object OrganizationDataAnalyticsApplication extends DataApplication {
  def main(args: Array[String]): Unit = {
    import spark.implicits._

    val start = LocalDateTime.now()

    val employeesDF = spark.read.json(empsDataPath).as[Employee]
    val depsDF = spark.read.json(depsDataPath).as[Dep]

    val employerDepartment = employeesDF.join(depsDF, "depNo")

    println("Departments hired most employees: ")
    employerDepartment
      .groupBy("depNo", "depName")
      .agg(count("*").as("employeesCount"))
      .orderBy(col("employeesCount").desc)
      .show()


    println("Departments hired most employees for the the past year: ")
    val pastYear = LocalDate
      .now()
      .minusYears(1)
      .format(DateTimeFormatter.ISO_DATE)

    employerDepartment
      .withColumn("dateHired", to_date(col("dateHired"), "yyyy-MM-dd"))
      .filter(col("dateHired") > lit(pastYear))
      .groupBy("depNo", "depName")
      .agg(count("*").as("employeesCount"))
      .orderBy(col("employeesCount").desc)
      .show()

    println("Top 5 departments with highest average salary: ")
    employerDepartment
      .groupBy("depNo", "depName")
      .agg(avg("salary").as("averageSalary"))
      .orderBy(col("averageSalary").desc)
      .limit(5)
      .show()

    val finish = LocalDateTime.now()
    val duration = Duration.between(start, finish)
    println(s"Job executed in: $duration")

    println("Press enter to exit")
    StdIn.readLine()
  }
}

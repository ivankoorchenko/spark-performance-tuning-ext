package capstone.solution

import capstone.{DataApplication, OrganizationDataSource}
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions._

/**
 * Spark application that syncs data from OrganizationDataSource to local storage, later to be used for analytics.
 */
object OrganizationDataSyncApplication extends DataApplication {
  def main(args: Array[String]): Unit = {
    val dataSource = new OrganizationDataSource

    import spark.implicits._

    val organizations = dataSource.employees
    val deps = dataSource.deps

    /**
     * After many attempts, I found that bucketing works  better in comparison with partitioning
     * (e.g. partitionBy("depNo")). Unfortunately, since DF bucketing is flat, I can't apply bucketing for
     * `depNo` and `dateHired` at the same time. I decided to go with `depNo` since it's more important for
     * the join at the analytics stage.
     *
     * Parquet format should be more efficient for IO in comparison to JSON, bit this was not a great success for some
     * reason.
     */
    organizations
      .toDF
      .write
      .bucketBy(100, "depNo")
      .mode(SaveMode.Overwrite)
      .saveAsTable("employees")

    deps
      .toDF
      .write
      .bucketBy(100, "depNo")
      .mode(SaveMode.Overwrite)
      .saveAsTable("deps")

    println("Data sync completed")
  }
}

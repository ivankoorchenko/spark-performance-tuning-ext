package capstone

import org.apache.spark.sql.SparkSession

trait DataApplication {
  val spark = SparkSession.builder()
    .config("spark.master", "local[*]")
    .config("hive.metastore.warehouse.dir", "spark-warehouse")
    .enableHiveSupport()
    .getOrCreate()

  spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

  val empsDataPath = "organization/data/emps"
  val depsDataPath = "organization/data/deps"
}

package capstone

import java.time.{Duration, LocalDateTime}

/**
 * Spark application that syncs data from OrganizationDataSource to local storage, later to be used for analytics.
 */
object OrganizationDataSyncApplication extends DataApplication {
  def main(args: Array[String]): Unit = {
    import spark.implicits._

    val start = LocalDateTime.now()

    val dataSource = new OrganizationDataSource
    val organizations = dataSource.employees
    val deps = dataSource.deps

    organizations.toDF().write.mode("overwrite").json(empsDataPath)
    deps.toDF().write.mode("overwrite").json(depsDataPath)

    println("Data sync completed")

    val finish = LocalDateTime.now()
    val duration = Duration.between(start, finish)
    println(s"Job executed in: $duration")
  }
}

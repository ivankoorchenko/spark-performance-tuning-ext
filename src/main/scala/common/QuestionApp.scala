package common

import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.io.StdIn

trait QuestionApp {
  val spark = SparkSession.builder()
    .config("spark.master", "local[*]")
    .getOrCreate()

  def main(args: Array[String]): Unit = {
    val dataFrame = run

    dataFrame.show()
    dataFrame.explain()

    StdIn.readLine("Press Enter to exit")
  }

  def readCsv(path: String) = {
    spark.read.option("header", "true").csv(s"src/main/resources/data/$path")
  }

  def run: DataFrame
}

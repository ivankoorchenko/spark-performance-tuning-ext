package part3.question3

import common.QuestionApp
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, count}
import part3.question3.Option1.spark

// 5s on MBP
object Option2 extends QuestionApp {
  override def run: DataFrame = {
    import spark.implicits._
    spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

    val employees: DataFrame = Employee.employees.toDF()
    val deps: DataFrame = Dep.deps.toDF()

    employees
      .repartition(col("depNo"))
      .join(deps.repartition(col("depNo")), "depNo")
      .where(col("depName") === "Innovation Lab")
      .agg(count("*"))
  }
}

package part3.question3

import common.QuestionApp
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{broadcast, col, count}
object Option3 extends QuestionApp {
  override def run: DataFrame = {
    import spark.implicits._
    spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

    val employees: DataFrame = Employee.employees.toDF().as("employees")
    val deps: DataFrame = Dep.deps.toDF().as("deps")

    def partitionColumn(df: DataFrame) = {
      df.withColumn("depPartition", col("depNo") % 24)
        .repartition(col("depPartition"))
    }

    partitionColumn(employees)
      .join(partitionColumn(deps), "depPartition")
      .where(col("employees.depNo") === col("deps.depNo"))
      .where(col("depName") === "Innovation Lab")
      .agg(count("*"))
  }
}

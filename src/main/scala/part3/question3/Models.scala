package part3.question3

case class Employee(id: Int, firstName: String, secondName: String, salary: Int, depNo: Int)

object Employee {
  val million = Math.pow(10, 6).toInt

  val employees: List[Employee] = {
    (1 to 1 * million).toList.flatMap { id =>
      List(
        Employee(id, s"first_name_$id", s"second_name_$id", (id % 10 + 5) * 10000, id % 100),
        Employee(id, s"first_name_$id", s"second_name_$id", (id % 10 + 5) * 10000, 1)
      )
    }
  }
}

case class Dep(depNo: Int, depName: String)

object Dep {
  val departmentNames = List(
    "Innovation Lab",
    "Customer Experience Division",
    "Operations Optimization Team",
    "Sustainability Initiatives Department",
    "Data Analytics and Insights Unit"
  )

  val deps: List[Dep] = (1 to 100).toList.map { id =>
    val name = departmentNames(departmentNames.size % 5)
    Dep(id, name)
  }
}



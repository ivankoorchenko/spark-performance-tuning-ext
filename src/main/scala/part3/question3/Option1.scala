package part3.question3

import common.QuestionApp
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

// ~9s on MBP
object Option1 extends QuestionApp {
  override def run: DataFrame = {
    import spark.implicits._
    spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

    val employees: DataFrame = Employee.employees.toDF()
    val deps: DataFrame = Dep.deps.toDF()

    employees
      .join(deps, "depNo")
      .where(col("depName") === "Innovation Lab")
      .agg(count("*"))
  }
}

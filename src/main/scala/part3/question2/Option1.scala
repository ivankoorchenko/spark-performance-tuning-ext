package part3.question2

import common.QuestionApp
import org.apache.spark.sql.{DataFrame, SaveMode}
import org.apache.spark.sql.functions._

object Option1 extends QuestionApp {
  override def run: DataFrame = {
    import spark.implicits._
    spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

    val employees: DataFrame = Employee.employees.toDF()
    val deps: DataFrame = Dep.deps.toDF()

    employees
      .write
      .mode(SaveMode.Overwrite)
      .bucketBy(5, "salary")
      .saveAsTable("employees")

    deps
      .write
      .mode(SaveMode.Overwrite)
      .bucketBy(5, "depNo")
      .saveAsTable("deps")

    val employeesDF = spark.read.table("employees")
    val depsDF = spark.read.table("deps")

    employeesDF
      .filter(col("salary") >= 100000)
      .join(depsDF, "depNo")
      .groupBy("depName")
      .agg(count("*"))
  }
}

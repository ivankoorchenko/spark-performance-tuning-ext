package part3.question2

import common.QuestionApp
import org.apache.spark.sql.functions.{col, count}
import org.apache.spark.sql.{DataFrame, SaveMode}

/**
== Physical Plan ==
 *(4) HashAggregate(keys=[depName#48], functions=[count(1)])
+- Exchange hashpartitioning(depName#48, 200), true, [id=#166]
   +- *(3) HashAggregate(keys=[depName#48], functions=[partial_count(1)])
      +- *(3) Project [depName#48]
         +- *(3) SortMergeJoin [depNo#41], [depNo#47], Inner
            :- *(1) Sort [depNo#41 ASC NULLS FIRST], false, 0
            :  +- *(1) Project [depNo#41]
            :     +- *(1) Filter ((isnotnull(salary#40) AND (salary#40 >= 100000)) AND isnotnull(depNo#41))
            :        +- *(1) ColumnarToRow
            :           +- FileScan parquet default.employees[salary#40,depNo#41] Batched: true, DataFilters: [isnotnull(salary#40), (salary#40 >= 100000), isnotnull(depNo#41)], Format: Parquet, Location: InMemoryFileIndex[file:/Users/ivan/Projects/gitlab/spark-performance-tuning-ext/spark-warehouse/e..., PartitionFilters: [], PushedFilters: [IsNotNull(salary), GreaterThanOrEqual(salary,100000), IsNotNull(depNo)], ReadSchema: struct<salary:int,depNo:int>, SelectedBucketsCount: 5 out of 5
            +- *(2) Sort [depNo#47 ASC NULLS FIRST], false, 0
               +- *(2) Project [depNo#47, depName#48]
                  +- *(2) Filter isnotnull(depNo#47)
                     +- *(2) ColumnarToRow
                        +- FileScan parquet default.deps[depNo#47,depName#48] Batched: true, DataFilters: [isnotnull(depNo#47)], Format: Parquet, Location: InMemoryFileIndex[file:/Users/ivan/Projects/gitlab/spark-performance-tuning-ext/spark-warehouse/d..., PartitionFilters: [], PushedFilters: [IsNotNull(depNo)], ReadSchema: struct<depNo:int,depName:string>, SelectedBucketsCount: 5 out of 5
 */
object Option3 extends QuestionApp {
  override def run: DataFrame = {
    import spark.implicits._

    spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

    val employees: DataFrame = Employee.employees.toDF()
    val deps: DataFrame = Dep.deps.toDF()

    employees
      .write
      .mode(SaveMode.Overwrite)
      .bucketBy(5, "depNo")
      .saveAsTable("employees")

    deps
      .write
      .mode(SaveMode.Overwrite)
      .bucketBy(5, "depNo")
      .saveAsTable("deps")

    val employeesDF = spark.read.table("employees")
    val depsDF = spark.read.table("deps")


    employeesDF
      .filter(col("salary") >= 100000)
      .join(depsDF, "depNo")
      .groupBy("depName")
      .agg(count("*"))
  }
}

package part3.question2

case class Employee(id: Int, firstName: String, secondName: String, salary: Int, depNo: Int)

object Employee {
  val employees: List[Employee] = (1 to 1000000).toList.map { id =>
    Employee(id, s"first_name_$id", s"second_name_$id", (id % 10 + 5) * 10000, id % 500)
  }
}

case class Dep(depNo: Int, depName: String)

object Dep {
  val deps: List[Dep] = (1 to 100).toList.map { id =>
    Dep(id, s"name_$id")
  }
}



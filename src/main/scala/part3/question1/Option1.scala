package part3.question1

import common.QuestionApp
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

/**
 * How can following job be optimized?
 * 1) no actions needed.
 * 2) Select depNo, firstName and secondName for `employeesDF`
 * 3) add column after join
 *
 */

object Option1 extends QuestionApp {
  override def run: DataFrame = {
    import spark.implicits._

    val employees: DataFrame = Employee.employees.toDF()
    val deps: DataFrame = Dep.deps.toDF()

    employees
      .withColumn("fullName", concat(employees("firstName"), employees("secondName")) )
      .join(deps, "depNo")
      .drop(employees("id"))
  }
}

package part3.question1

import common.QuestionApp
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.concat
import part3.question1.Option1.spark

object Option2 extends QuestionApp {
  override def run: DataFrame = {
    import spark.implicits._

    val employees: DataFrame = Employee.employees.toDF()
    val deps: DataFrame = Dep.deps.toDF()

    employees
      .join(deps, "depNo")
      .withColumn("fullName", concat(employees("firstName"), employees("secondName")))
  }
}

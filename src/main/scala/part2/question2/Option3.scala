package part2.question2

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

object Option3 extends Question2App {
  override def run: DataFrame = {
    val employeesDF = readEmployeesDF
    employeesDF
      .filter(employeesDF("date_left").isNull)
      .agg(sum("salary"))
      .limit(1)
  }
}

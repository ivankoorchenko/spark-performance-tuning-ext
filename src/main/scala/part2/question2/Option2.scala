package part2.question2

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

object Option2 extends Question2App {
  override def run: DataFrame = {
    val employeesDF = readEmployeesDF
    val departmentsDF = readDepartmentsDF
    employeesDF
      .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
      .filter(length(employeesDF("date_left")) !== 0)
      .groupBy(departmentsDF("id"), departmentsDF("dep_name"))
      .agg(count("*").as("dep_people_left"))
      .orderBy(desc("dep_people_left"))
      .limit(1)
  }
}

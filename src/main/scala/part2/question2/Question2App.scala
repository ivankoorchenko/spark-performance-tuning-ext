package part2.question2

import common.QuestionApp


trait Question2App extends QuestionApp {
  def readEmployeesDF = readCsv("question_2/emp.csv")

  def readDepartmentsDF = readCsv("question_2/deps.csv")
}

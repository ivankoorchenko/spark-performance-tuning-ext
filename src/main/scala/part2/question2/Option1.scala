package part2.question2

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SparkSession

import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.IntegerType

object Option1 extends Question2App {
  override def run: DataFrame = {
    val employeesDF = readEmployeesDF
    val departmentsDF = readDepartmentsDF
    employeesDF
      .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
      .groupBy(departmentsDF("id"), departmentsDF("dep_name"))
      .agg(sum("salary").cast(IntegerType).as("dep_budget"))
      .orderBy(desc("dep_budget"))
      .limit(1)
  }
}

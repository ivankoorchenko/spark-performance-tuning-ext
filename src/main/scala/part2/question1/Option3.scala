package part2.question1

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

object Option3 extends Question1App {
  override def run: DataFrame = {
    val employeesDF = readEmployeesDF
    val window = Window.partitionBy("dep_no").orderBy(desc("salary"))

    employeesDF
      .withColumn("salary_rank", rank().over(window))
      .where(col("salary_rank") <= 2)
      .drop("salary_rank")
  }
}

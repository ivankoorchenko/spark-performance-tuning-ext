package part2.question1

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

object Option2 extends Question1App {
  override def run: DataFrame = {
    val employeesDF = readEmployeesDF
    val departmentsDF = readDepartmentsDF
    employeesDF
      .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
      .groupBy(departmentsDF("id"), departmentsDF("dep_name"))
      .agg(count(employeesDF("id")).as("dep_size"))
      .orderBy(desc("dep_size"))
      .limit(1)
      .select("dep_name")
  }
}

package part2.question1

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.IntegerType

object Option1 extends Question1App {

  override def run: DataFrame = {
    val employeesDF = readEmployeesDF
    val departmentsDF = readDepartmentsDF
    employeesDF
      .join(departmentsDF, employeesDF("dep_no") === departmentsDF("id"))
      .groupBy(departmentsDF("id"))
      .agg(avg("salary").cast(IntegerType))
  }
}

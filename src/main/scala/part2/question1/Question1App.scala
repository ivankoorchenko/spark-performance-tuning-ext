package part2.question1

import common.QuestionApp
import org.apache.spark.sql.DataFrame

trait Question1App extends QuestionApp {
  def readEmployeesDF: DataFrame = readCsv("question_1/emp.csv")

  def readDepartmentsDF: DataFrame = readCsv("question_1/deps.csv")
}
